cc.Class({
    extends: cc.Component,

    properties: {
        lablist:{
            default:[],
            type:cc.Label
        },
        
        labIsuse:{
            default:null,
            type:cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {

    },

    init:function(data){
        for(var i =0; i<data.nums.length;i++)
        {
            try {
                this.lablist[i].string = data.nums[i];
            } catch (error) {
                cc.log("chat public_raward:"+error);
            }
            
        }
        this.labIsuse.string = data.isuse;
    }

});
