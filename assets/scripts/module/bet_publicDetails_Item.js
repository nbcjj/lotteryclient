/**
 * !#zh 奖项栏组件
 * @information 奖级，中奖条件，奖金
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labName:{
            default: null,
            type: cc.Label
        },

        labAmount:{
            default: null,
            type: cc.Label
        },

        labValue:{
            default: null,
            type: cc.Label
        },

        _data:null  //数据信息
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labName.string = this._data.name;
            this.labValue.string = this._data.value;
            this.labAmount.string = this._data.amount;
        }
    },

    /** 
    * 接收奖项数据
    * @method init
    * @param {Object} ret
    */
    init:function(ret){
        this._data = ret;
    }

});
