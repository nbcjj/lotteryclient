cc.Class({
    extends: cc.Component,

    properties: {

    },

    // use this for initialization
    onLoad: function () {
    },


    //是否显示-遗漏数组-显示控件列表
    setMiss:function(isMiss,toggleList,missArry){
        if(isMiss)
        {
            if(missArry.length == toggleList.length)
            {
                if(missArry.length > 0)
                {
                    for(var i=0;i<toggleList.length;i++)
                    {
                        toggleList[i].getComponent('Bet_toggle_ball').setMissNum(missArry[i]);
                    }
                }
                else
                    toggleList[i].getComponent('Bet_toggle_ball').setMissNum("--");
            }  
        }
        else
        {
            for(var i=0;i<toggleList.length;i++)
            {
                toggleList[i].getComponent('Bet_toggle_ball').setMissNum("");
            }
        }   
    },

    //是否显示-遗漏数组-显示控件列表
    setMissArry:function(isMiss,toggleList,missArry){
        if(isMiss)
        {
            var arrtemp = [];
            if(missArry.length > 5)
            {
                arrtemp.push(missArry);
            }
            else
                arrtemp = missArry;
            if(arrtemp.length > 0)
            {
                for(var i=0;i<arrtemp.length;i++)
                {
                    for(var j=0;j<toggleList[i].length;j++)
                    {
                        toggleList[i][j].getComponent('Bet_toggle_ball').setMissNum(arrtemp[i][j]);
                    } 
                }
            }
            else
            {
                for(var i=0;i<toggleList.length;i++)
                {
                    for(var j=0;j<toggleList[j].length;j++)
                    {
                        toggleList[i][j].getComponent('Bet_toggle_ball').setMissNum("--");
                    } 
                }
            }
        }
        else
        {
            for(var i=0;i<toggleList.length;i++)
            {
                for(var j=0;j<toggleList[i].length;j++)
                {
                    toggleList[i][j].getComponent('Bet_toggle_ball').setMissNum("");
                }
            }
        }   
    },

    //是否显示-遗漏数组-显示控件列表-彩种id-规则id
    showMiss:function(toggleList,lotteryid,playCode){
        var recv = function(ret){
            var temparr = [];
            if(ret.Code === 0)
            {
                temparr = ret.Data;
                try {
                    var obj = eval('('+temparr+')');
                    this.setMiss(true,toggleList,obj["o"]);
                } catch (error) {
                    cc.log("betManage erro1:"+error);
                }
               
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);  
                this.setMiss(true,toggleList,temparr);
            }
        }.bind(this);
        var data = {
            LotteryCode:lotteryid,
            IsuseName:"1",
            PlayCode:playCode,
            TopCount:100
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETLOTTERYMISS, data, recv.bind(this),"POST");   
    },

    showMissArry:function(toggleArryList,lotteryid,playCode){
        var recv = function(ret){
            var temparr = [];
            if(ret.Code === 0)
            {
                temparr = ret.Data;
                
                var obj = eval('('+temparr+')');
                this.setMissArry(true,toggleArryList,obj["o"]);
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);  
                this.setMissArry(true,toggleArryList,temparr);
            }
        }.bind(this);
        var data = {
            LotteryCode:lotteryid,
            IsuseName:"1",
            PlayCode:playCode,
            TopCount:100
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETLOTTERYMISS, data, recv.bind(this),"POST");   
    },

    showK3Miss:function(toggleList,lotteryid,playCode,isArry){
        var recv = function(ret){
            var temparr = [];
            if(ret.Code === 0)
            {
                temparr = ret.Data;
                var obj = eval('('+temparr+')');
                if(isArry)   
                    this.setK3MissArry(true,toggleList,obj["o"]);
                else
                    this.setK3Miss(true,toggleList,obj["o"]);
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);  
                if(isArry) 
                    this.setK3MissArry(true,toggleList,temparr);
                else
                    this.setK3Miss(true,toggleList,temparr);
            }
        }.bind(this);
        var data = {
            LotteryCode:lotteryid,
            IsuseName:"1",
            PlayCode:playCode,
            TopCount:100
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETLOTTERYMISS, data, recv.bind(this),"POST");   
    },

    setK3Miss:function(isMiss,toggleList,missArry){
        if(isMiss)
        {
            if(missArry.length > 0)
            {
                for(var i=0;i<toggleList.length;i++)
                {
                    var tog = toggleList[i].node.getChildByName("labMiss");
                    if(tog)
                    {
                        tog.getComponent(cc.Label).string = missArry[i];
                        tog.active = true;
                    }
                }
            }
            else
            {
                for(var i=0;i<toggleList.length;i++)
                {
                    var tog = toggleList[i].node.getChildByName("labMiss");
                    if(tog)
                    {
                        tog.getComponent(cc.Label).string = "--";
                        tog.active = true;
                    }
                }
            }
        }
        else
        {
           for(var i=0;i<toggleList.length;i++)
            {
                var tog = toggleList[i].node.getChildByName("labMiss");
                if(tog)
                {
                    tog.getComponent(cc.Label).string = "";
                    tog.active = false;
                }
            }
        }
    },

    setK3MissArry:function(isMiss,toggleList,missArry){
        if(isMiss)
        {
            if(missArry.length > 0)
            {
                for(var i=0;i<toggleList.length;i++)
                {
                    for(var j=0;j<toggleList[i].length;j++)
                    {
                        var tog = toggleList[i][j].node.getChildByName("labMiss");
                        if(tog)
                        {
                            tog.getComponent(cc.Label).string = missArry[i][j];
                            tog.active = true;
                        }   
                    }
                }
            }
            else
            {
                for(var i=0;i<toggleList.length;i++)
                {
                    for(var j=0;j<toggleList[i].length;j++)
                    {
                        var tog = toggleList[i][j].node.getChildByName("labMiss");
                        if(tog)
                        {
                            tog.getComponent(cc.Label).string = "--";
                            tog.active = true;
                        }
                    }
                }
            }
        }
        else
        {
           for(var i=0;i<toggleList.length;i++)
            {
                for(var j=0;j<toggleList[i].length;j++)
                {
                    var tog = toggleList[i][j].node.getChildByName("labMiss");
                    if(tog)
                    {
                        tog.getComponent(cc.Label).string = "";
                        tog.active = false;
                    }
                }
            }
        }
    }

});
