/*
对应彩种分析界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        newsPaperPrefab: cc.Prefab,  //新闻标题列表预制
        newsPanel: cc.Node,     
        labHead: cc.Label   //标题
    },

    // use this for initialization
    onLoad: function () {

    },

    init: function(home,tem){
        if(home.chatRoomPlayId == "801"){
            this.labHead.string = '双色球彩种分析';
        }else if(home.chatRoomPlayId == "901"){
            this.labHead.string = '大乐透彩种分析';    
        }
        this._homeType =home;

        for(var i=0;i<tem.Data.length;++i){
            var newPaper =cc.instantiate(this.newsPaperPrefab);
            var tData ={
                Code: tem.Code,
                Data: tem.Data[i],
                Msg: tem.Msg
            }
            newPaper.getComponent('newsPaper').init(tem.Data[i].Title,tem.Data[i].Time,this._homeType,tData,this);
            this.newsPanel.addChild(newPaper);   
            
            //--滚动视图的高度，根据列表的多少来决定
            this.newsPanel.parent.parent.height = (tem.Data.length*newPaper.height)+100+10;
            this.newsPanel.parent.height = (tem.Data.length*newPaper.height)+100+10;
        }
    },

    //关闭
    onClose:function(event){
        this.node.getComponent("Page").backAndRemove();
    }

});
