/*
* 重置密码界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        edCode:{
            default: null,
            type: cc.EditBox
        },

        labPrompt:{
            default: null,
            type: cc.Label
        },

        labTopTitle: {
            default: null,
            type: cc.Label
        },

        _verifycode:"",
        _type:0,//修改的类型
        _pwd:""
    },

    // use this for initialization
    onLoad: function () {
        this._pwd = "";
    },
    init:function(data){
        this._verifycode = data.Code;
        this._type = data.type;
        if(data.type == 1)
        {
            this._verifycode = data.Code;
            this.labPrompt.string = "请为您的账号设置新的登录密码";
            this.labTopTitle.string = "修改登录密码";
            this.edCode.maxLength = 18;
            //this.edCode.inputMode = EditBox.InputMode.SINGLE_LINE;
        }
        else if(data.type == 2)
        {
            this._verifycode = data.Code;
            this.labPrompt.string = "请为您的账号设置新的提现密码";
            this.labTopTitle.string = "修改提现密码";
            this.edCode.maxLength = 6;
            //this.edCode.inputMode = EditBox.InputMode.NUMERIC;
        }
    },

    onTextChangedPwd:function(text, editbox, customEventData){
        var txt = text;
        if(this._type == 2) //提现密码
        {
            if(Utils.isInt(txt)||txt == "")
                this._pwd = txt;
            else
            {
                txt = this._pwd;
                editbox.string = txt;
                return;
            }
        }
    },
    
    checkInput:function(){
        if(this.edCode.string != "")
        {
            if(!Utils.isPassWord(this.edCode.string))
            {
                ComponentsUtils.showTips("密码格式错误！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("密码不能为空！");
            return false;
        }
        return true;
    },

    onTrue: function(){
        if(this.checkInput())
        {
            var recv = function(ret){
                ComponentsUtils.unblock();
                if (ret.Code !== 0) {
                    cc.log(ret.Msg);
                    ComponentsUtils.showTips(ret.Msg);
                }else{
                    if(this._type == 1)
                    {
                        User.setPwd(this.edCode.string);
                        this.onOut();
                    }  
                    else if(this._type == 2)
                        User.setIsWithdrawPwd(true);    
                    this.onClose();
                }
            }.bind(this);

            var data = {
                Token: User.getLoginToken(),
                UserCode: User.getUserCode(),
                PwdType: this._type,//1登录密码 2支付密码
                Pwd: this.edCode.string,
                VerifyCode: this._verifycode,
            };
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.RESETPWDLOGIN, data, recv.bind(this),"POST");  
            ComponentsUtils.block();
        }
    },

    onOut: function(){
        User.setIsvermify(false);
        var recv = function recv(ret) { 
            ComponentsUtils.unblock();
            if(ret.Code == 0 )
            {
                User.setLoginToken(ret.Token);
                var callback = function callback(ret1) {
              //      cc.log("login callback");
                    if(ret1 && ret1 == true)
                    {
                        window.Notification.emit("onGotoUser","");
                    }   
                    else
                        window.Notification.emit("onGotoHall","");
                }
                
                CL.MANAGECENTER.gotoLoginPop(callback.bind(this));
                this.onClose();
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            Equipment:Utils.getEquipment(),
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.OUTACCOUNT, data, recv.bind(this),"POST");    
        ComponentsUtils.block();
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }
    
});
