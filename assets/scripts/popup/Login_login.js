/*
* 登录界面
*/
cc.Class({
    extends: cc.Component,

    properties: {
        edAccount: {
            default: null,
            type: cc.EditBox
        },
        edPassword: {
            default: null,
            type: cc.EditBox
        },

        fbRegister:{ 
            default: null,
            type: cc.Prefab
        },

        fbForgetPassword:{ 
            default: null,
            type: cc.Prefab
        },

        fbResetPassword : { 
            default: null,
            type: cc.Prefab
        },

        fbRegisterVenify : { 
            default: null,
            type: cc.Prefab
        },  

        fbModifyPasswordSuccess : { 
            default: null,
            type: cc.Prefab
        },

        btnQQLogin:cc.Node,
        btnWeichatLogin:cc.Node,
        labDec:cc.Node,

        _callBack:null,
        _wxLogin:false
    },

    setCallBack: function(callBack)
    {
        this._callBack = callBack;
    },

    // use this for initialization
    onLoad: function () {  
        if(User.getAppState() == 0)
        {
            this.btnQQLogin.active = false;
            this.btnWeichatLogin.active = false;
            this.labDec.active = false;
        }

        if(User.getTel() != "");
        {
            this.edAccount.string = User.getTel();
        }

        window.Notification.on("onRegisterVerify",function(data){
            this.onRegisterVerify(data);
        },this);
        window.Notification.on("onResetPassword",function(data){
            this.onResetPassword(data);
        },this);
        window.Notification.on("onPasswordSuccess",function(data){
            this.onPasswordSuccess(data);
        },this);
        window.Notification.on("onCloseLogin",function(data){
            this.onCloseBtn(data);
        },this);
    },

    onTextChangePwd:function(text, editbox, customEventData){
        var txt = text;
        if(txt == "")
        {
            this.pwd = txt;
        }
        else
        {
            var re = txt.replace(/\s+/g,"");
            editbox.string = re;
            txt = re;
            this.pwd = txt;
        }
    },

    onTextChangedPhone:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.phoneNum = txt;
        else
        {
            txt = this.phoneNum;
            editbox.string = txt;
            return;
        }
    },


    //登录
    onLogin: function(){
        if(Utils.checkInput(this.edAccount.string,this.edPassword.string,null,null,true))
        {
            var self = this;
            var recv = function recv(ret) {
                ComponentsUtils.unblock();
                if (ret.Code !== 0 && ret.Code !== 100) {
                        cc.error(ret.Msg);
                        ComponentsUtils.showTips(ret.Msg);
                    } else {
                        User.upDateUserInfo(false,ret.Data);
                        User.setLoginToken(ret.Data.Token);
                        User.setIsvermify(true);
                        User.setLoginMode(2);
                        User.setPwd(this.edPassword.string);
                        User.setUid(ret.Data.Uid);
                        
                        if(ret.Code !== 100)
                        {
                            CL.NET.login();
                        }
                            
                        ComponentsUtils.showTips("登录成功");
                        this.onCloseBtn(true);
                    }
                }.bind(this);

                var data = {
                    Equipment: Utils.getEquipment(),
                    SourceType: Utils.getSourceType(),
                    Mobile:this.edAccount.string,
                    Pwd:this.edPassword.string
                }; 
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.ACCOUNTLOGIN, data, recv.bind(this),"POST");   
            ComponentsUtils.block();  
        }
    },

    //QQ登录
    onQQLogin:function(){
        ComponentsUtils.showTips("暂不支持QQ登录！");
    },

    //微信登录
    onWechatLogin:function(){
        ComponentsUtils.showTips("暂不支持微信登录！");
    },

    //忘记密码
    onForgetPassWord: function(data){
        var canvas = cc.find("Canvas");
        var fbForgetPasswordPop = cc.instantiate(this.fbForgetPassword);
        canvas.addChild(fbForgetPasswordPop);
    },

    //马上注册
    onRegister:function(data){
        var canvas = cc.find("Canvas");
        var fbRegisterPop = cc.instantiate(this.fbRegister);
        canvas.addChild(fbRegisterPop);
    },

    //注册验证
    onRegisterVerify:function(data){
        var canvas = cc.find("Canvas");
        var fbRegisterVenifyPop = cc.instantiate(this.fbRegisterVenify);
        fbRegisterVenifyPop.getComponent("Login_registerVerify").initData(data);
        canvas.addChild(fbRegisterVenifyPop);
    },  

    //重置密码
    onResetPassword:function(data){
        var canvas = cc.find("Canvas");
        var fbResetPasswordPop = cc.instantiate(this.fbResetPassword);
        fbResetPasswordPop.getComponent("Login_resetPassword").initData(data);
        canvas.addChild(fbResetPasswordPop);
    },  

    //重置密码成功
    onPasswordSuccess:function(data){
        var canvas = cc.find("Canvas");
        var fbModifyPasswordSuccessPop = cc.instantiate(this.fbModifyPasswordSuccess);
        canvas.addChild(fbModifyPasswordSuccessPop);
    },  

    onClose:function(){
        window.Notification.offType("onRegisterVerify");
        window.Notification.offType("onResetPassword");
        window.Notification.offType("onPasswordSuccess");
        window.Notification.offType("onCloseLogin");
        this.node.getComponent("Page").backAndRemove();
        if(this._callBack != null)
            this._callBack(false);
    },

    onCloseBtn:function(data){
        window.Notification.offType("onRegisterVerify");
        window.Notification.offType("onResetPassword");
        window.Notification.offType("onPasswordSuccess");
        window.Notification.offType("onCloseLogin");
        if(this._callBack != null)
            this._callBack(true);
        this.node.getComponent("Page").backAndRemove();
    }  

});
