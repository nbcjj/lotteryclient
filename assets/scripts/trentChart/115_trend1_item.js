/**
 * !#zh 11选5走势图走势统计组件 
 * @information 走势统计
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labNums:{
            default:[],
            type:cc.Label
        },

        labDec:{
            default:null,
            type:cc.Label
        },

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            this.labDec.string = this._data.dec;
            this.labDec.node.color = this._data.numColor;
            for(var i=0;i<this._data.nums.length;i++)
            {
                this.labNums[i].node.color = this._data.numColor;
                this.labNums[i].string = this._data.nums[i].toString();
            }
        }
    },

    /** 
    * 接收走势图走势统计信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }
    
});
